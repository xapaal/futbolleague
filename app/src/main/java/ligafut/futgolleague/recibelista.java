package ligafut.futgolleague;

import android.util.JsonReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergi on 11/06/2016.
 */
public class recibelista {
    private String local;
    private String visitante;
    private String goleslocal;
    private String golesvisitante;

    public recibelista(String local, String visitante, String goleslocal,String golesvisitante) {
        this.local = local;
        this.visitante= visitante;
        this.goleslocal = goleslocal;
        this.golesvisitante = golesvisitante;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getVisitante() {
        return visitante;
    }

    public List leerLista(JsonReader reader) throws IOException {
        // Lista temporal
        ArrayList lista = new ArrayList();

        reader.beginArray();
        while (reader.hasNext()) {
            // Leer objeto
            lista.add(leerlista(reader));
        }
        reader.endArray();
        return lista;
    }
    public recibelista leerlista(JsonReader reader) throws IOException {
        String especie = null;
        String descripcion = null;
        String imagen = null;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case "local":
                    especie = reader.nextString();
                    break;
                case "visitante":
                    descripcion = reader.nextString();
                    break;
                case "goleslocal":
                    imagen = reader.nextString();
                    break;
                case "golevisitante":
                    imagen = reader.nextString();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return new recibelista(local,visitante,goleslocal,golesvisitante);
    }


}