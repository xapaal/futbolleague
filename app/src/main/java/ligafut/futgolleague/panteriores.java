package ligafut.futgolleague;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class panteriores extends AppCompatActivity {

    ListView lista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panteriores);

        //Json server_call = new Json();
        //server_call.execute();

        //new JSONParse().execute();

        lista = (ListView) findViewById(R.id.listView);

        PeticioHttp peticio = new PeticioHttp(panteriores.this);
        peticio.execute();
    }

    private class PeticioHttp extends AsyncTask<String, Void, String> {

        private Activity activity;

        public PeticioHttp(Activity act) { activity =act;}

        @Override
        protected String doInBackground(String... params) {
            String respuesta = "";
            String miRespuesta = null;

            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://"+getString(R.string.ip_host_direction)+":9000/Application/PartidosAnterioresAndroid");

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                miRespuesta = readStream(response.getEntity().getContent());
                System.out.println("Respuesta Servidor: " + miRespuesta);
                return miRespuesta;
            }
            catch (Exception e) {
                e.printStackTrace();
                respuesta = e.getMessage();
                return respuesta;
            }



        }

        @Override
        protected void onPostExecute(String response) {

            ArrayList<partitAnterior> partidosAnteriores = new ArrayList<partitAnterior>();
            Gson gson = new Gson();
            JsonElement json = new JsonParser().parse(response);
            JsonArray array = json.getAsJsonArray();
            Iterator<JsonElement> iterator = array.iterator();
            int c = 0;
            while (iterator.hasNext()) {
                JsonElement json2 = (JsonElement) iterator.next();
                partitAnterior object = (partitAnterior) gson.fromJson(json2, partitAnterior.class);
                partidosAnteriores.add(object);
            }

            for (int i = 0; i<partidosAnteriores.size(); i++) {
                partitAnterior pa = partidosAnteriores.get(i);
                //start inflating the list
            }

            Log.d("LoginScreen", "Log in response: "+response);


            PartidosAnterioresAdapter adaptador = new PartidosAnterioresAdapter(getApplicationContext(), partidosAnteriores);
            lista.setAdapter(adaptador);
        }


    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;

        String result = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
            return result;


        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }

    }

    public class partitAnterior {
        public List<String> equipos;
        public String local;
        public String visitante;
        public int goleslocal;
        public int golesvisitante;
        public int id;


        public partitAnterior(List<String> eq, String local, String vis, int gol1, int gol2, int id) {
            this.equipos = eq;
            this.local = local;
            this.visitante = vis;
            this.goleslocal = gol1;
            this.golesvisitante = gol2;
            this.id = id;
        }


    }

    class PartidosAnterioresAdapter extends ArrayAdapter<partitAnterior> {

        List<partitAnterior> datos;
        Context ctx;

        public PartidosAnterioresAdapter(Context context, ArrayList<partitAnterior> datos) {
            super(context, R.layout.item_partidos_anteriores, datos);
            this.datos = datos;
            this.ctx = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater factory = getLayoutInflater();

            final View item = factory.inflate(R.layout.item_partidos_anteriores, null);

            TextView equiposText = (TextView) item.findViewById(R.id.text_equipos);
            TextView localText = (TextView) item.findViewById(R.id.text_local);
            TextView visitanteText = (TextView) item.findViewById(R.id.text_visit);
            TextView golesLocText = (TextView) item.findViewById(R.id.text_gol_local);
            TextView golesVisText = (TextView) item.findViewById(R.id.text_gol_vis);
            TextView idText = (TextView) item.findViewById(R.id.text_id);


            //for (int i = 0; i<datos.get(position).equipos.size(); i++)
             //   equiposText.setText(equiposText.getText()+datos.get(position).equipos.get(i) +"\n");

            Log.d("insertingList", "inserting pos "+position);

            localText.setText(datos.get(position).local);
            visitanteText.setText(datos.get(position).visitante);
            golesLocText.setText(datos.get(position).goleslocal+"");
            golesVisText.setText(datos.get(position).golesvisitante+"");
            idText.setText(datos.get(position).id+"");

            return (item);
        }
    }

    }

