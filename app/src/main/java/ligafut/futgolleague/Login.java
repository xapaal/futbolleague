package ligafut.futgolleague;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Login extends Activity {


   // public final static String EXTRA_MESSAGE = "com.ligafut.futgolleague.MESSAGE";
    EditText name;
    EditText pass;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        name = (EditText) findViewById(R.id.txtUsuario);
        pass = (EditText) findViewById(R.id.txtPass);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://ligafut.futgolleague/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://ligafut.futgolleague/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    private class PeticioHttp extends AsyncTask<String, Void, String> {

      private Activity activity;

        public PeticioHttp(Activity act) { activity =act;}

        @Override
        protected String doInBackground(String... params) {
            String respuesta = "";
            String miRespuesta = null;

            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://"+getString(R.string.ip_host_direction)+":9000/Application/loginAndroid");

                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("password", params[1]));
                nameValuePairs.add(new BasicNameValuePair("name", params[0]));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                miRespuesta = readStream(response.getEntity().getContent());
                System.out.println("Respuesta Servidor: " + miRespuesta);
                return miRespuesta;
            }
            catch (Exception e) {
                e.printStackTrace();
                respuesta = e.getMessage();
                return respuesta;
            }



        }

        @Override
        protected void onPostExecute(String response) {

            name.setEnabled(true);
            pass.setEnabled(true);

            Log.d("LoginScreen", "Log in response: "+response);

            if (response.equals("1")) {
                Intent intent = new Intent(Login.this, Inicio.class);
                Log.d("LoginScreen", "puting extra: "+name.getText().toString());
                intent.putExtra("name_user", name.getText().toString());
                Login.this.startActivity(intent);
                finish();
            }
            else{
                Toast toast = Toast.makeText(Login.this, "Usuario o contraseña incorrectos", Toast.LENGTH_LONG);
                View view = toast.getView();
                view.setBackgroundColor(Color.parseColor("#880000"));
                TextView text = (TextView) view.findViewById(android.R.id.message);
                text.setPadding(5, 5, 5, 5);
        /*here you can do anything with text*/
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

    public void Registrar(View view) {
        Intent intent = new Intent(this, Registrar.class);
      /*  EditText editText = (EditText) findViewById(R.id.txtUsuario);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);*/
        startActivity(intent);

    }

    public void inicio(View view) {

        name.setEnabled(false);
        pass.setEnabled(false);

        PeticioHttp peticio = new PeticioHttp(Login.this);
       peticio.execute(new String[]{name.getText().toString(), pass.getText().toString()});

/*
            if (nombre.length() > 0 && password.length() > 0) {



            } else
                Toast.makeText(this, "Campo Nombre o Contraseña vacio",
                        Toast.LENGTH_LONG).show();


*/
    }


    private String readStream(InputStream in) {
        BufferedReader reader = null;

        String result = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
            return result;


        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }

}
}

