package ligafut.futgolleague;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Clasificacion extends AppCompatActivity {

    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clasificacion);

        lista = (ListView) findViewById(R.id.listView);

        PeticioHttp peticio = new PeticioHttp(Clasificacion.this);
        peticio.execute();
    }


    private class PeticioHttp extends AsyncTask<String, Void, String> {

        private Activity activity;

        public PeticioHttp(Activity act) { activity =act;}

        @Override
        protected String doInBackground(String... params) {
            String respuesta = "";
            String miRespuesta = null;

            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://"+getString(R.string.ip_host_direction)+":9000/Application/ClasificacionAndroid");

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                miRespuesta = readStream(response.getEntity().getContent());
                System.out.println("Respuesta Servidor: " + miRespuesta);
                return miRespuesta;
            }
            catch (Exception e) {
                e.printStackTrace();
                respuesta = e.getMessage();
                return respuesta;
            }



        }

        @Override
        protected void onPostExecute(String response) {

            ArrayList<Clasification> classifs = new ArrayList<Clasification>();
            Gson gson = new Gson();
            JsonElement json = new JsonParser().parse(response);
            JsonArray array = json.getAsJsonArray();
            Iterator<JsonElement> iterator = array.iterator();
            int c = 0;
            while (iterator.hasNext()) {
                JsonElement json2 = (JsonElement) iterator.next();
                Clasification object = (Clasification) gson.fromJson(json2, Clasification.class);
                classifs.add(object);
            }

            for (int i = 0; i<classifs.size(); i++) {
                Clasification pa = classifs.get(i);
                //start inflating the list
            }

            Log.d("LoginScreen", "Log in response: " + response);


            ClasificacionAdapter adaptador = new ClasificacionAdapter(getApplicationContext(), classifs);
            lista.setAdapter(adaptador);
        }


    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;

        String result = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
            return result;


        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }

    }

    public class Clasification {
        String nombrequipo;
        int puntos;
        int partidosjugados;
        int victorias;
        int empates;
        int derrotas;
        int golesfavor;
        int golescontra;


        public Clasification(String name, int puntos, int pj, int pg, int pe, int pp, int gf, int gc) {
            this.nombrequipo = name;
            this.puntos = puntos;
            this.partidosjugados = pj;
            this.victorias = pg;
            this.empates = pe;
            this.derrotas = pp;
            this.golesfavor = gf;
            this.golescontra = gc;
        }


    }

    class ClasificacionAdapter extends ArrayAdapter<Clasification> {

        List<Clasification> datos;
        Context ctx;

        public ClasificacionAdapter(Context context, ArrayList<Clasification> datos) {
            super(context, R.layout.item_partidos_anteriores, datos);
            this.datos = datos;
            this.ctx = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater factory = getLayoutInflater();

            final View item = factory.inflate(R.layout.item_clasificaciones, null);

            TextView equiposText = (TextView) item.findViewById(R.id.text_equipos);
            TextView puntosText = (TextView) item.findViewById(R.id.text_puntos);
            TextView pjText = (TextView) item.findViewById(R.id.text_pj);
            TextView pgText = (TextView) item.findViewById(R.id.text_pg);
            TextView peText = (TextView) item.findViewById(R.id.text_pe);
            TextView ppText = (TextView) item.findViewById(R.id.text_pp);
            TextView gfText = (TextView) item.findViewById(R.id.text_gf);
            TextView gcText = (TextView) item.findViewById(R.id.text_gc );


            equiposText.setText(datos.get(position).nombrequipo);
            puntosText.setText(datos.get(position).puntos+"");
            pjText.setText(datos.get(position).partidosjugados +"");
            pgText.setText(datos.get(position).victorias+"");
            peText.setText(datos.get(position).empates+"");
            ppText.setText(datos.get(position).derrotas+"");
            gfText.setText(datos.get(position).golesfavor+"");
            gcText.setText(datos.get(position).golescontra+"");



            Log.d("insertingList", "inserting pos " + position);

            return (item);
        }
    }

}
