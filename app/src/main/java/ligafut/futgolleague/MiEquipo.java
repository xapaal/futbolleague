package ligafut.futgolleague;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MiEquipo extends AppCompatActivity {

    public String name_user;
    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_equipo);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        lista = (ListView) findViewById(R.id.listView);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            name_user = extras.getString("name_user");
        }



        PeticioHttp peticio = new PeticioHttp(this);
        peticio.execute(new String[]{name_user});

    }

    private class PeticioHttp extends AsyncTask<String, Void, String> {

        private Activity activity;

        public PeticioHttp(Activity act) { activity =act;}

        @Override
        protected String doInBackground(String... params) {
            String respuesta = "";
            String miRespuesta = null;

            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://"+getString(R.string.ip_host_direction)+":9000/Application/MiEquipoAndroid");

                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                //nameValuePairs.add(new BasicNameValuePair("password", params[1]));
                nameValuePairs.add(new BasicNameValuePair("user", params[0]));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                miRespuesta = readStream(response.getEntity().getContent());
                System.out.println("Respuesta Servidor: " + miRespuesta);
                return miRespuesta;
            }
            catch (Exception e) {
                e.printStackTrace();
                respuesta = e.getMessage();
                return respuesta;
            }



        }

        @Override
        protected void onPostExecute(String response) {

            ArrayList<equipo> partidosAnteriores = new ArrayList<equipo>();
            Gson gson = new Gson();
            JsonElement json = new JsonParser().parse(response);
            JsonArray array = json.getAsJsonArray();
            Iterator<JsonElement> iterator = array.iterator();
            int c = 0;
            while (iterator.hasNext()) {
                JsonElement json2 = (JsonElement) iterator.next();
                equipo object = (equipo) gson.fromJson(json2, equipo.class);
                partidosAnteriores.add(object);
            }

            for (int i = 0; i<partidosAnteriores.size(); i++) {
                equipo pa = partidosAnteriores.get(i);
                //start inflating the list
            }

            Log.d("LoginScreen", "Log in response: "+response);


            PartidosAnterioresAdapter adaptador = new PartidosAnterioresAdapter(getApplicationContext(), partidosAnteriores);
            lista.setAdapter(adaptador);
        }


    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;

        String result = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
            return result;


        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }

    }

    public class equipo {
        public String name;
        public int goles;
        public int tarjetas;

        public equipo(String n, int g, int t) {
            this.name = n;
            this.goles = g;
            this.tarjetas = t;
        }


    }

    class PartidosAnterioresAdapter extends ArrayAdapter<equipo> {

        List<equipo> datos;
        Context ctx;

        public PartidosAnterioresAdapter(Context context, ArrayList<equipo> datos) {
            super(context, R.layout.item_equipo, datos);
            this.datos = datos;
            this.ctx = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater factory = getLayoutInflater();

            final View item = factory.inflate(R.layout.item_equipo, null);

            TextView nameText = (TextView) item.findViewById(R.id.text_nombre);
            TextView golesText = (TextView) item.findViewById(R.id.text_goles);
            TextView tarjetasText = (TextView) item.findViewById(R.id.text_tajetas);

            Log.d("insertingList", "inserting pos " + position + ", name: " + datos.get(position).name);

            nameText.setText(datos.get(position).name);
            golesText.setText(datos.get(position).goles+"");
            tarjetasText.setText(datos.get(position).tarjetas+"");

            return (item);
        }
    }

}

